﻿using System;
using System.Collections.Generic;
using Bisection_Solver.Model;
using Bisection_Solver.View;

namespace Bisection_Solver.Presenter
{
    class BisectionPresenter
    {
        private CloseMethodSolver closeMethod;
        private NewtonRaphson newtonRaphson;
        private readonly IMainView view;

        public BisectionPresenter(NewtonRaphson newtonRaphson, CloseMethodSolver closeMethod, IMainView view)
        {
            this.closeMethod = closeMethod;
            this.newtonRaphson = newtonRaphson;
            this.view = view;
            view.onEvaluateClosedMethod += onEvaluateClosedMethod;
            view.onEvaluateOpenMethod += onEvaluateNewtonRaphson;
        }
        public void onEvaluateClosedMethod(object sender, EventArgs e)
        {
            closeMethod = view.GetModelClosed();
            List<string> results = closeMethod.GetResultList();
            view.GenerateResult(results, closeMethod.ResultSentence, closeMethod);
        }

        public void onEvaluateNewtonRaphson(object sender, EventArgs e)
        {
            newtonRaphson = view.GetModelOpen();
            newtonRaphson.Evaluate();
            List<string> results = newtonRaphson.Results;
            view.GenerateResult(results, newtonRaphson.ResultString, newtonRaphson);
        }
    }
}