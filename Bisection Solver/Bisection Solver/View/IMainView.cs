﻿using System;
using System.Collections.Generic;
using Bisection_Solver.Model;

namespace Bisection_Solver.View
{
    interface IMainView
    {
        event EventHandler onEvaluateClosedMethod;
        event EventHandler onEvaluateOpenMethod;

        //Closed Method Fields
        double XuClosed { get; }
        double XiClosed { get; }
        double LimitErrorClosed { get; }
        int IterationClosed { get; }
        string DefinedFunctionClosed { get;}


        //Open Method Fields
        double XiOpen { get; }
        double LimitErrorOpen { get; }
        int IterationOpen { get; }
        string DefinedFunctionOpen { get; }

        CloseMethodSolver GetModelClosed();
        NewtonRaphson GetModelOpen();

        void GenerateResult(List<string> results, string resultString, CloseMethodSolver methodSolver);
        void GenerateResult(List<string> results, string resultString, NewtonRaphson methodSolver);


    }
}
