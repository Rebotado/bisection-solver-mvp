﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using MetroFramework.Forms;
using org.mariuszgromada.math.mxparser;

using System.Windows.Forms.DataVisualization.Charting;
using Bisection_Solver.Model;

namespace Bisection_Solver.View
{
    public partial class TableGraph : MetroForm
    {
        private List<string> dataSource { get; set; }
        private CloseMethodSolver CloseMethod { get; set; }
        private NewtonRaphson NewtonRaphson { get; set; }
        private string resultString { get; set; }


        public TableGraph(List<string> dataSource, CloseMethodSolver closeMethod, string resultString)
        {
             this.resultString = resultString;
            this.dataSource = dataSource;
             this.CloseMethod = closeMethod;

            InitializeComponent();
            chart1.MouseWheel += chart1_MouseWheel;
        }

        public TableGraph(List<string> dataSource, NewtonRaphson newtonRaphson, string resultString)
        {
            this.resultString = resultString;
            this.NewtonRaphson = newtonRaphson;
            this.dataSource = dataSource;

            InitializeComponent();
            chart1.MouseWheel += chart1_MouseWheel;
        }


        private void TableGraphView_Load(object sender, EventArgs e)
        {
            GenerateLabels();
            SetGraphType();
            GenerateTable();
            GenerateGraph();
            AutoSizeTable();
        }

        private void GenerateLabels()
        {
            if (NewtonRaphson == null)
            {
                functionLabel.Text += CloseMethod.DefinedFunction.getFunctionExpressionString();
                xiLabel.Text += CloseMethod.Xi.ToString();
                xuLabel.Text += CloseMethod.Xu.ToString();
                iterationLabel.Text += CloseMethod.Iteration.ToString();
                errorLabel.Text += CloseMethod.ApproximateError.ToString();
                resultLabel.Text = resultString;
            }
            else
            {
                functionLabel.Text += NewtonRaphson.Function.getFunctionExpressionString();
                xiLabel.Text += NewtonRaphson.Xi.ToString();
                xuLabel.Hide();
                iterationLabel.Text += NewtonRaphson.Iteration.ToString();
                errorLabel.Text += NewtonRaphson.LimitError.ToString();
                resultLabel.Text += resultString;
            }
        }

        private void GenerateTable()
        {
            foreach (var stringResult in dataSource)
            {
                var row = stringResult.Split('|').Where(s => !s.Equals("")).ToList().ToArray();
                resultGrid.Rows.Add(row);
            }
        }

        private void SetGraphType()
        {
            if(CloseMethod is Bisection)
            {
                resultGrid.Columns.Add("contador" , "Contador");
                resultGrid.Columns.Add("xi", "Xi");
                resultGrid.Columns.Add("xu", "Xu");
                resultGrid.Columns.Add("fxi", "f(Xi)");
                resultGrid.Columns.Add("xr", "Xr");
                resultGrid.Columns.Add("fxr", "f(Xr)");
                resultGrid.Columns.Add("signo", "Signo");
                resultGrid.Columns.Add("errorAproximado", "Error Aproximado");
            }
            else if(CloseMethod is FalsePosition)
            {
                resultGrid.Columns.Add("contador", "Contador");
                resultGrid.Columns.Add("xi", "Xi");
                resultGrid.Columns.Add("fxi", "f(Xi)");
                resultGrid.Columns.Add("xu", "Xu");
                resultGrid.Columns.Add("fxu", "f(Xu)");
                resultGrid.Columns.Add("xr", "Xr");
                resultGrid.Columns.Add("fxr", "f(Xr)");
                resultGrid.Columns.Add("signo", "Signo");
                resultGrid.Columns.Add("errorAproximado", "Error Aproximado");
            }
            else
            {
                resultGrid.Columns.Add("contador", "Contador");
                resultGrid.Columns.Add("xi","Xi");
                resultGrid.Columns.Add("fxi","f(xi)");
                resultGrid.Columns.Add("derfxi","f'(xi)");
                resultGrid.Columns.Add("error","Error Aproximado");
            }
        }

        private void GenerateGraph()
        {
            List<Coordinate> coordinates = new List<Coordinate>();
            coordinates = GenerateCoordinates();
            double closestToZero = GetClosestToZero(coordinates);
            double minX = Math.Floor(coordinates.Min(n => n.X));
            double maxX = Math.Ceiling(coordinates.Max(n => n.X));
            double minY = Math.Floor(coordinates.Min(n => n.Y));
            double maxY = Math.Ceiling(coordinates.Max(n => n.Y));
            AutoSizeGraph(minX, maxX, minY, maxY);

            foreach (var coordinate in coordinates)
            {
                if ((coordinate.Y >= -0.1 && coordinate.Y <= 0.1 && coordinate.Y == closestToZero))
                {
                    if(coordinate.X == 0 && coordinate.Y == 0)
                        chart1.Series["Coordinates2"].Points.AddXY(0.001, coordinate.Y); //Chart cant plot (0,0) so plot (0,001,0) instead
                    else
                    chart1.Series["Coordinates2"].Points.AddXY(coordinate.X, coordinate.Y);
                }
                else
                    chart1.Series["Coordinates"].Points.AddXY(coordinate.X, coordinate.Y);
            }

            if(CloseMethod is FalsePosition)
            {
                Function function = CloseMethod.DefinedFunction;
                Coordinate xiCoordinates = new Coordinate(CloseMethod.LastXi, function.calculate(CloseMethod.LastXi));
                Coordinate xuCoordinates = new Coordinate(CloseMethod.LastXu, function.calculate(CloseMethod.LastXu));
                chart1.Series["Coordinates3"].Points.AddXY(xiCoordinates.X, xiCoordinates.Y);
                chart1.Series["Coordinates3"].Points.AddXY(xuCoordinates.X, xuCoordinates.Y);
            }
            else if(CloseMethod == null)
            {
                Function function = NewtonRaphson.Function;
                Expression derived = NewtonRaphson.DerivedFunction;
                derived.setArgumentValue("x",NewtonRaphson.PreviousXi);
                Coordinate previousXi = new Coordinate(NewtonRaphson.PreviousXi, derived.calculate());
                Coordinate lastXi = new Coordinate(NewtonRaphson.LastXi, function.calculate(NewtonRaphson.LastXi));
                chart1.Series["Coordinates3"].Points.AddXY(previousXi.X, previousXi.Y);
                chart1.Series["Coordinates3"].Points.AddXY(lastXi.X, lastXi.Y);

            }
        }

        private double GetClosestToZero(List<Coordinate> numbers)
        {
            double closestToZero = double.PositiveInfinity;
            for(int i = 0; i < numbers.Count - 1; i++)
            {
                if (numbers[i].Y == double.NaN)
                    continue;
                closestToZero = Math.Abs(numbers[i].Y) < Math.Abs(closestToZero) ? numbers[i].Y : closestToZero;
            }
            return closestToZero;
        }

        private List<Coordinate> GenerateCoordinates()
        {
            List<Coordinate> coordinates = new List<Coordinate>();
            if (NewtonRaphson == null)
            {
                for (double c = CloseMethod.Xi; c < CloseMethod.Xu; c += 0.01)
                {
                    double y = CloseMethod.DefinedFunction.calculate(Math.Round(c, 2));
                    double x = Math.Round(c, 2);
                    coordinates.Add(new Coordinate(x, y));
                }
            }
            else
            {
                for(double c = NewtonRaphson.Xi; c < NewtonRaphson.LastXi * 2; c += 0.01)
                {
                    double y = NewtonRaphson.Function.calculate(Math.Round(c, 2));
                    double x = Math.Round(c, 2);
                    coordinates.Add(new Coordinate(x, y));
                }
            }
            return coordinates;
        }

        private void AutoSizeTable()
        {
            for (int i = 0; i <= resultGrid.Columns.Count - 1; i++)
            {
                resultGrid.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            }
        }

        private void AutoSizeGraph(double minX, double maxX, double minY, double maxY)
        {
            ChartArea CA = chart1.ChartAreas[0];
            CA.AxisX.Minimum = minX <= 1 && minX >= -1 ? -5 : Math.Floor(minX);
            CA.AxisX.Maximum = maxX <= 1 && maxX >= -1 ? 5 : Math.Ceiling(maxX);
            CA.AxisY.Minimum = minY <= 1 && minY >= -1 ? -5 : Math.Floor(minY);
            CA.AxisY.Maximum = maxY <= 1 && maxY >= -1 ? 5 : Math.Ceiling(maxY);

            CA.AxisY.Interval = Math.Round(maxY / 10);
            CA.AxisX.Interval = Math.Round(maxX / 10);

            CA.AxisX.ScaleView.Zoomable = true;
            CA.CursorX.IsUserSelectionEnabled = true;
            CA.AxisY.ScaleView.Zoomable = true;
            CA.CursorY.IsUserSelectionEnabled = true;

        }



        //Zoom with mouse wheel
        // Reference: https://stackoverflow.com/questions/13584061/how-to-enable-zooming-in-microsoft-chart-control-by-using-mouse-wheel
        private void chart1_MouseWheel(object sender, MouseEventArgs e)
        {
            var chart = (Chart)sender;
            var xAxis = chart.ChartAreas[0].AxisX;
            var yAxis = chart.ChartAreas[0].AxisY;

            try
            {
                if (e.Delta < 0) // Scrolled down.
                {
                    xAxis.ScaleView.ZoomReset();
                    yAxis.ScaleView.ZoomReset();
                }
                else if (e.Delta > 0) // Scrolled up.
                {
                    var xMin = xAxis.ScaleView.ViewMinimum;
                    var xMax = xAxis.ScaleView.ViewMaximum;
                    var yMin = yAxis.ScaleView.ViewMinimum;
                    var yMax = yAxis.ScaleView.ViewMaximum;

                    var posXStart = Math.Floor(xAxis.PixelPositionToValue(e.Location.X) - (xMax - xMin) / 2.5);
                    var posXFinish = Math.Ceiling(xAxis.PixelPositionToValue(e.Location.X) + (xMax - xMin) / 2.5);
                    var posYStart = Math.Floor(yAxis.PixelPositionToValue(e.Location.Y) - (yMax - yMin) / 2.5);
                    var posYFinish = Math.Ceiling(yAxis.PixelPositionToValue(e.Location.Y) + (yMax - yMin) / 2.5);


                    xAxis.ScaleView.Zoom(posXStart, posXFinish);
                    yAxis.ScaleView.Zoom(posYStart, posYFinish);
                }
            }
            catch { }
        }
    }
}
