﻿namespace Bisection_Solver.View
{
    partial class TableGraph
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.resultGrid = new System.Windows.Forms.DataGridView();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.functionLabel = new MetroFramework.Controls.MetroLabel();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.resultLabel = new MetroFramework.Controls.MetroTextBox();
            this.errorLabel = new MetroFramework.Controls.MetroLabel();
            this.iterationLabel = new MetroFramework.Controls.MetroLabel();
            this.xuLabel = new MetroFramework.Controls.MetroLabel();
            this.xiLabel = new MetroFramework.Controls.MetroLabel();
            ((System.ComponentModel.ISupportInitialize)(this.resultGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.metroPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // resultGrid
            // 
            this.resultGrid.AllowUserToAddRows = false;
            this.resultGrid.BackgroundColor = System.Drawing.SystemColors.Window;
            this.resultGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.resultGrid.Location = new System.Drawing.Point(23, 75);
            this.resultGrid.Name = "resultGrid";
            this.resultGrid.ReadOnly = true;
            this.resultGrid.Size = new System.Drawing.Size(645, 247);
            this.resultGrid.TabIndex = 0;
            // 
            // chart1
            // 
            chartArea1.AxisX.Interval = 1D;
            chartArea1.AxisX.Maximum = 10D;
            chartArea1.AxisX.Minimum = -10D;
            chartArea1.AxisY.Interval = 1D;
            chartArea1.AxisY.IsLabelAutoFit = false;
            chartArea1.AxisY.Maximum = 10D;
            chartArea1.AxisY.Minimum = -10D;
            chartArea1.Name = "ChartArea1";
            chartArea1.Position.Auto = false;
            chartArea1.Position.Height = 90F;
            chartArea1.Position.Width = 70F;
            chartArea1.Position.X = 3F;
            chartArea1.Position.Y = 3F;
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(674, 75);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Legend = "Legend1";
            series1.Name = "Coordinates";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
            series2.Legend = "Legend1";
            series2.Name = "Coordinates2";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Legend = "Legend1";
            series3.Name = "Coordinates3";
            this.chart1.Series.Add(series1);
            this.chart1.Series.Add(series2);
            this.chart1.Series.Add(series3);
            this.chart1.Size = new System.Drawing.Size(629, 493);
            this.chart1.TabIndex = 1;
            this.chart1.Text = "chart1";
            // 
            // functionLabel
            // 
            this.functionLabel.AutoSize = true;
            this.functionLabel.Location = new System.Drawing.Point(14, 65);
            this.functionLabel.Name = "functionLabel";
            this.functionLabel.Size = new System.Drawing.Size(44, 19);
            this.functionLabel.TabIndex = 3;
            this.functionLabel.Text = "f(x) = ";
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.resultLabel);
            this.metroPanel1.Controls.Add(this.errorLabel);
            this.metroPanel1.Controls.Add(this.iterationLabel);
            this.metroPanel1.Controls.Add(this.xuLabel);
            this.metroPanel1.Controls.Add(this.xiLabel);
            this.metroPanel1.Controls.Add(this.functionLabel);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(23, 328);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(645, 216);
            this.metroPanel1.TabIndex = 4;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // resultLabel
            // 
            // 
            // 
            // 
            this.resultLabel.CustomButton.Image = null;
            this.resultLabel.CustomButton.Location = new System.Drawing.Point(599, 2);
            this.resultLabel.CustomButton.Name = "";
            this.resultLabel.CustomButton.Size = new System.Drawing.Size(37, 37);
            this.resultLabel.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.resultLabel.CustomButton.TabIndex = 1;
            this.resultLabel.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.resultLabel.CustomButton.UseSelectable = true;
            this.resultLabel.CustomButton.Visible = false;
            this.resultLabel.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.resultLabel.Lines = new string[0];
            this.resultLabel.Location = new System.Drawing.Point(3, 3);
            this.resultLabel.MaxLength = 32767;
            this.resultLabel.Multiline = true;
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.PasswordChar = '\0';
            this.resultLabel.ReadOnly = true;
            this.resultLabel.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.resultLabel.SelectedText = "";
            this.resultLabel.SelectionLength = 0;
            this.resultLabel.SelectionStart = 0;
            this.resultLabel.ShortcutsEnabled = true;
            this.resultLabel.Size = new System.Drawing.Size(639, 42);
            this.resultLabel.TabIndex = 8;
            this.resultLabel.UseSelectable = true;
            this.resultLabel.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.resultLabel.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // errorLabel
            // 
            this.errorLabel.AutoSize = true;
            this.errorLabel.Location = new System.Drawing.Point(232, 94);
            this.errorLabel.Name = "errorLabel";
            this.errorLabel.Size = new System.Drawing.Size(124, 19);
            this.errorLabel.TabIndex = 7;
            this.errorLabel.Text = "Error Aproximado: ";
            // 
            // iterationLabel
            // 
            this.iterationLabel.AutoSize = true;
            this.iterationLabel.Location = new System.Drawing.Point(232, 65);
            this.iterationLabel.Name = "iterationLabel";
            this.iterationLabel.Size = new System.Drawing.Size(66, 19);
            this.iterationLabel.TabIndex = 6;
            this.iterationLabel.Text = "Iteracion: ";
            // 
            // xuLabel
            // 
            this.xuLabel.AutoSize = true;
            this.xuLabel.Location = new System.Drawing.Point(14, 125);
            this.xuLabel.Name = "xuLabel";
            this.xuLabel.Size = new System.Drawing.Size(31, 19);
            this.xuLabel.TabIndex = 5;
            this.xuLabel.Text = "Xu: ";
            // 
            // xiLabel
            // 
            this.xiLabel.AutoSize = true;
            this.xiLabel.Location = new System.Drawing.Point(14, 94);
            this.xiLabel.Name = "xiLabel";
            this.xiLabel.Size = new System.Drawing.Size(27, 19);
            this.xiLabel.TabIndex = 4;
            this.xiLabel.Text = "Xi: ";
            // 
            // TableGraph
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1174, 580);
            this.Controls.Add(this.resultGrid);
            this.Controls.Add(this.metroPanel1);
            this.Controls.Add(this.chart1);
            this.Name = "TableGraph";
            this.Text = "Resultados";
            this.Load += new System.EventHandler(this.TableGraphView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.resultGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView resultGrid;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private MetroFramework.Controls.MetroLabel functionLabel;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroLabel errorLabel;
        private MetroFramework.Controls.MetroLabel iterationLabel;
        private MetroFramework.Controls.MetroLabel xuLabel;
        private MetroFramework.Controls.MetroLabel xiLabel;
        private MetroFramework.Controls.MetroTextBox resultLabel;
    }
}