﻿using System;
using System.Collections.Generic;
using MetroFramework.Forms;
using Bisection_Solver.View;
using Bisection_Solver.Model;
using System.Text.RegularExpressions;
using MetroFramework.Controls;
using System.Windows.Forms;
using System.Diagnostics;

namespace Bisection_Solver
{
    public partial class MainView : MetroForm , IMainView

    {
        public event EventHandler onEvaluateClosedMethod;
        public event EventHandler onEvaluateOpenMethod;

        private TableGraph resultView;


        //Closed Method Fields
        public int IterationClosed
        {
            get { return Convert.ToInt32(iterationTxtBox.Text); }
        }

        public double XuClosed { get { return Convert.ToDouble(xuTxtBox.Text); } }

        public double XiClosed { get { return Convert.ToDouble(xiTxtBox.Text); } }

        public double LimitErrorClosed { get { return Convert.ToDouble(errorTxtBox.Text); } }

        public string DefinedFunctionClosed { get { return FixInput(functionTxtBox.Text); } }

        //Newton Rhapson Fields
        public double XiOpen { get { return Convert.ToDouble(xiTxtBoxOpen.Text); } }
        public double LimitErrorOpen { get { return Convert.ToDouble(errorTxtBoxOpen.Text); } }
        public string DefinedFunctionOpen { get { return FixInput(functionTxtBoxOpen.Text); } }
        public int IterationOpen { get { return Convert.ToInt32(iterationTxtBoxOpen.Text); } }



        public MainView()
        {
            InitializeComponent();
        }



        public CloseMethodSolver GetModelClosed()
        {
            if (methodSelector.SelectedIndex == 0)
                return new Bisection(XiClosed, XuClosed, LimitErrorClosed, DefinedFunctionClosed, IterationClosed);
            else
                return new FalsePosition(XiClosed, XuClosed, LimitErrorClosed, DefinedFunctionClosed, IterationClosed);
        }

        public NewtonRaphson GetModelOpen()
        {
            return new NewtonRaphson(DefinedFunctionOpen, IterationOpen, LimitErrorOpen, XiOpen);
        }

        private void EvaluateButton_Click(object sender, EventArgs e)
        {
            onEvaluateClosedMethod(sender, e);
        }

        public void GenerateResult(List<string> results, string resultString, CloseMethodSolver methodSolver)
        {
            resultView?.Close();
            resultView = new TableGraph(results, methodSolver, resultString);
            resultView.Show();
        }

        public void GenerateResult(List<string> results, string resultString, NewtonRaphson methodSolver)
        {
            resultView?.Close();
            resultView = new TableGraph(results, methodSolver, resultString);
            resultView.Show();
        }

        private void IterationToggle_CheckedChanged(object sender, EventArgs e)
        {
            if(iterationToggle.Checked)
            {
                iterationTxtBox.Text = "";
                iterationTxtBox.Show();
                iterationLabel.Show();
            }
            else
            {
                iterationTxtBox.Text = "100";
                iterationTxtBox.Hide();
                iterationLabel.Hide();
            }
        }

        private void ErrorToggle_CheckedChanged(object sender, EventArgs e)
        {
            if (errorToggle.Checked)
            {
                errorTxtBox.Text = "";
                errorTxtBox.Show();
                errorLabel.Show();
            }
            else
            {
                errorTxtBox.Text = "0.05";
                errorTxtBox.Hide();
                errorLabel.Hide();
            }
        }

        private string FixInput(string input)
        {
            string input2 = Regex.Replace(input, @"([a-zA-Z][x])", m => string.Format(@"{0}(x)", m.Value.Substring(0, m.Length - 1)));
            return "f(x) = " + Regex.Replace(input2, @"(\d[x])", m => string.Format(@"{0}*x", m.Value.Substring(0, m.Length - 1)));
            //Matches al x that have a coefficient, then formats it to: number*x.
        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {
            var textBox = sender as MetroTextBox;

            Regex regex = new Regex(@"[^0-9\+-\.]");
            MatchCollection matches = regex.Matches(textBox.Text);
            if(matches.Count > 0)
            {
                System.Windows.Forms.MessageBox.Show("Solo se admiten numeros");
                textBox.Text = Regex.Replace(textBox.Text, @"[^0-9\+-\.]", "");
            }
        }

        private void MainView_Load(object sender, EventArgs e)
        {
            methodSelector.SelectedIndex = 0;
            methodSelectorOpen.SelectedIndex = 0;
        }

        private void EvaluateButtonOpen_Click(object sender, EventArgs e)
        {


            foreach (Control control in this.Controls)
            {
                if(control is MetroTabControl)
                {
                    var tab = (MetroTabControl)control;
                    foreach(Control tabControl in tab.TabPages)
                    {
                        foreach(Control tabPageControl in tabControl.Controls)
                        {
                            Debug.WriteLine(tabPageControl);
                            if (tabPageControl is MetroTextBox)
                            {
                                if (((MetroTextBox)tabPageControl).Text == String.Empty)
                                {
                                    MessageBox.Show("Llena todos los campos", "Campos vacios");
                                    return;
                                }
                            }
                        }
                    }
                }

            }
            onEvaluateOpenMethod(sender, e);
        }

        private void IterationToggleOpen_CheckedChanged(object sender, EventArgs e)
        {
            if (iterationToggleOpen.Checked)
            {
                iterationTxtBoxOpen.Text = "";
                iterationTxtBoxOpen.Show();
                iterationLabelO.Show();
            }
            else
            {
                iterationTxtBoxOpen.Text = "100";
                iterationTxtBoxOpen.Hide();
                iterationLabelO.Hide();
            }
        }

        private void ErrorToggleOpen_CheckedChanged(object sender, EventArgs e)
        {
            if (errorToggleOpen.Checked)
            {
                errorTxtBoxOpen.Text = "";
                errorTxtBoxOpen.Show();
                errorLabelOpen.Show();
            }
            else
            {
                errorTxtBoxOpen.Text = "0.05";
                errorTxtBoxOpen.Hide();
                errorLabelOpen.Hide();
            }
        }


    }

}
