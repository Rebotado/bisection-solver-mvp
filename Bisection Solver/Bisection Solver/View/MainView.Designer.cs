﻿namespace Bisection_Solver
{
    partial class MainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.functionTxtBox = new MetroFramework.Controls.MetroTextBox();
            this.xiTxtBox = new MetroFramework.Controls.MetroTextBox();
            this.iterationTxtBox = new MetroFramework.Controls.MetroTextBox();
            this.xuTxtBox = new MetroFramework.Controls.MetroTextBox();
            this.errorTxtBox = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.iterationLabel = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.errorLabel = new MetroFramework.Controls.MetroLabel();
            this.evaluateButton = new MetroFramework.Controls.MetroButton();
            this.iterationToggle = new MetroFramework.Controls.MetroToggle();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.errorToggle = new MetroFramework.Controls.MetroToggle();
            this.methodSelector = new MetroFramework.Controls.MetroComboBox();
            this.methodSelectorNR = new MetroFramework.Controls.MetroTabControl();
            this.tabPage1 = new MetroFramework.Controls.MetroTabPage();
            this.tabPage2 = new MetroFramework.Controls.MetroTabPage();
            this.evaluateButtonOpen = new MetroFramework.Controls.MetroButton();
            this.iterationTxtBoxOpen = new MetroFramework.Controls.MetroTextBox();
            this.functionTxtBoxOpen = new MetroFramework.Controls.MetroTextBox();
            this.xiTxtBoxOpen = new MetroFramework.Controls.MetroTextBox();
            this.errorTxtBoxOpen = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.errorLabelOpen = new MetroFramework.Controls.MetroLabel();
            this.metroLabel12 = new MetroFramework.Controls.MetroLabel();
            this.iterationLabelO = new MetroFramework.Controls.MetroLabel();
            this.methodSelectorOpen = new MetroFramework.Controls.MetroComboBox();
            this.W = new MetroFramework.Controls.MetroLabel();
            this.errorToggleOpen = new MetroFramework.Controls.MetroToggle();
            this.iterationLabelOpen = new MetroFramework.Controls.MetroLabel();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.iterationToggleOpen = new MetroFramework.Controls.MetroToggle();
            this.methodSelectorNR.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // functionTxtBox
            // 
            // 
            // 
            // 
            this.functionTxtBox.CustomButton.Image = null;
            this.functionTxtBox.CustomButton.Location = new System.Drawing.Point(104, 1);
            this.functionTxtBox.CustomButton.Name = "";
            this.functionTxtBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.functionTxtBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.functionTxtBox.CustomButton.TabIndex = 1;
            this.functionTxtBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.functionTxtBox.CustomButton.UseSelectable = true;
            this.functionTxtBox.CustomButton.Visible = false;
            this.functionTxtBox.Lines = new string[] {
        "-0.5x^2 + 2.5x + 4.5"};
            this.functionTxtBox.Location = new System.Drawing.Point(137, 23);
            this.functionTxtBox.MaxLength = 32767;
            this.functionTxtBox.Name = "functionTxtBox";
            this.functionTxtBox.PasswordChar = '\0';
            this.functionTxtBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.functionTxtBox.SelectedText = "";
            this.functionTxtBox.SelectionLength = 0;
            this.functionTxtBox.SelectionStart = 0;
            this.functionTxtBox.ShortcutsEnabled = true;
            this.functionTxtBox.Size = new System.Drawing.Size(126, 23);
            this.functionTxtBox.TabIndex = 0;
            this.functionTxtBox.Text = "-0.5x^2 + 2.5x + 4.5";
            this.functionTxtBox.UseSelectable = true;
            this.functionTxtBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.functionTxtBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // xiTxtBox
            // 
            // 
            // 
            // 
            this.xiTxtBox.CustomButton.Image = null;
            this.xiTxtBox.CustomButton.Location = new System.Drawing.Point(104, 1);
            this.xiTxtBox.CustomButton.Name = "";
            this.xiTxtBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.xiTxtBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.xiTxtBox.CustomButton.TabIndex = 1;
            this.xiTxtBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.xiTxtBox.CustomButton.UseSelectable = true;
            this.xiTxtBox.CustomButton.Visible = false;
            this.xiTxtBox.Lines = new string[] {
        "5"};
            this.xiTxtBox.Location = new System.Drawing.Point(137, 63);
            this.xiTxtBox.MaxLength = 32767;
            this.xiTxtBox.Name = "xiTxtBox";
            this.xiTxtBox.PasswordChar = '\0';
            this.xiTxtBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.xiTxtBox.SelectedText = "";
            this.xiTxtBox.SelectionLength = 0;
            this.xiTxtBox.SelectionStart = 0;
            this.xiTxtBox.ShortcutsEnabled = true;
            this.xiTxtBox.Size = new System.Drawing.Size(126, 23);
            this.xiTxtBox.TabIndex = 1;
            this.xiTxtBox.Text = "5";
            this.xiTxtBox.UseSelectable = true;
            this.xiTxtBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.xiTxtBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.xiTxtBox.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            // 
            // iterationTxtBox
            // 
            // 
            // 
            // 
            this.iterationTxtBox.CustomButton.Image = null;
            this.iterationTxtBox.CustomButton.Location = new System.Drawing.Point(104, 1);
            this.iterationTxtBox.CustomButton.Name = "";
            this.iterationTxtBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.iterationTxtBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iterationTxtBox.CustomButton.TabIndex = 1;
            this.iterationTxtBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iterationTxtBox.CustomButton.UseSelectable = true;
            this.iterationTxtBox.CustomButton.Visible = false;
            this.iterationTxtBox.Lines = new string[] {
        "5"};
            this.iterationTxtBox.Location = new System.Drawing.Point(137, 146);
            this.iterationTxtBox.MaxLength = 32767;
            this.iterationTxtBox.Name = "iterationTxtBox";
            this.iterationTxtBox.PasswordChar = '\0';
            this.iterationTxtBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iterationTxtBox.SelectedText = "";
            this.iterationTxtBox.SelectionLength = 0;
            this.iterationTxtBox.SelectionStart = 0;
            this.iterationTxtBox.ShortcutsEnabled = true;
            this.iterationTxtBox.Size = new System.Drawing.Size(126, 23);
            this.iterationTxtBox.TabIndex = 3;
            this.iterationTxtBox.Text = "5";
            this.iterationTxtBox.UseSelectable = true;
            this.iterationTxtBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iterationTxtBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.iterationTxtBox.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            // 
            // xuTxtBox
            // 
            // 
            // 
            // 
            this.xuTxtBox.CustomButton.Image = null;
            this.xuTxtBox.CustomButton.Location = new System.Drawing.Point(104, 1);
            this.xuTxtBox.CustomButton.Name = "";
            this.xuTxtBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.xuTxtBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.xuTxtBox.CustomButton.TabIndex = 1;
            this.xuTxtBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.xuTxtBox.CustomButton.UseSelectable = true;
            this.xuTxtBox.CustomButton.Visible = false;
            this.xuTxtBox.Lines = new string[] {
        "10"};
            this.xuTxtBox.Location = new System.Drawing.Point(137, 106);
            this.xuTxtBox.MaxLength = 32767;
            this.xuTxtBox.Name = "xuTxtBox";
            this.xuTxtBox.PasswordChar = '\0';
            this.xuTxtBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.xuTxtBox.SelectedText = "";
            this.xuTxtBox.SelectionLength = 0;
            this.xuTxtBox.SelectionStart = 0;
            this.xuTxtBox.ShortcutsEnabled = true;
            this.xuTxtBox.Size = new System.Drawing.Size(126, 23);
            this.xuTxtBox.TabIndex = 2;
            this.xuTxtBox.Text = "10";
            this.xuTxtBox.UseSelectable = true;
            this.xuTxtBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.xuTxtBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.xuTxtBox.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            // 
            // errorTxtBox
            // 
            // 
            // 
            // 
            this.errorTxtBox.CustomButton.Image = null;
            this.errorTxtBox.CustomButton.Location = new System.Drawing.Point(104, 1);
            this.errorTxtBox.CustomButton.Name = "";
            this.errorTxtBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.errorTxtBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.errorTxtBox.CustomButton.TabIndex = 1;
            this.errorTxtBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.errorTxtBox.CustomButton.UseSelectable = true;
            this.errorTxtBox.CustomButton.Visible = false;
            this.errorTxtBox.Lines = new string[] {
        "3"};
            this.errorTxtBox.Location = new System.Drawing.Point(137, 187);
            this.errorTxtBox.MaxLength = 32767;
            this.errorTxtBox.Name = "errorTxtBox";
            this.errorTxtBox.PasswordChar = '\0';
            this.errorTxtBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.errorTxtBox.SelectedText = "";
            this.errorTxtBox.SelectionLength = 0;
            this.errorTxtBox.SelectionStart = 0;
            this.errorTxtBox.ShortcutsEnabled = true;
            this.errorTxtBox.Size = new System.Drawing.Size(126, 23);
            this.errorTxtBox.TabIndex = 4;
            this.errorTxtBox.Text = "3";
            this.errorTxtBox.UseSelectable = true;
            this.errorTxtBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.errorTxtBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.errorTxtBox.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(16, 23);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(54, 19);
            this.metroLabel1.TabIndex = 5;
            this.metroLabel1.Text = "Funcion";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(14, 63);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(20, 19);
            this.metroLabel2.TabIndex = 6;
            this.metroLabel2.Text = "Xi";
            // 
            // iterationLabel
            // 
            this.iterationLabel.AutoSize = true;
            this.iterationLabel.Location = new System.Drawing.Point(14, 146);
            this.iterationLabel.Name = "iterationLabel";
            this.iterationLabel.Size = new System.Drawing.Size(71, 19);
            this.iterationLabel.TabIndex = 8;
            this.iterationLabel.Text = "Iteraciones";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(14, 106);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(24, 19);
            this.metroLabel4.TabIndex = 7;
            this.metroLabel4.Text = "Xu";
            // 
            // errorLabel
            // 
            this.errorLabel.AutoSize = true;
            this.errorLabel.Location = new System.Drawing.Point(14, 187);
            this.errorLabel.Name = "errorLabel";
            this.errorLabel.Size = new System.Drawing.Size(117, 19);
            this.errorLabel.TabIndex = 9;
            this.errorLabel.Text = "Error Aproximado";
            // 
            // evaluateButton
            // 
            this.evaluateButton.Location = new System.Drawing.Point(188, 277);
            this.evaluateButton.Name = "evaluateButton";
            this.evaluateButton.Size = new System.Drawing.Size(141, 23);
            this.evaluateButton.TabIndex = 10;
            this.evaluateButton.Text = "Evaluar";
            this.evaluateButton.UseSelectable = true;
            this.evaluateButton.Click += new System.EventHandler(this.EvaluateButton_Click);
            // 
            // iterationToggle
            // 
            this.iterationToggle.AutoSize = true;
            this.iterationToggle.Checked = true;
            this.iterationToggle.CheckState = System.Windows.Forms.CheckState.Checked;
            this.iterationToggle.Location = new System.Drawing.Point(565, 79);
            this.iterationToggle.Name = "iterationToggle";
            this.iterationToggle.Size = new System.Drawing.Size(80, 17);
            this.iterationToggle.TabIndex = 11;
            this.iterationToggle.Text = "On";
            this.iterationToggle.UseSelectable = true;
            this.iterationToggle.CheckedChanged += new System.EventHandler(this.IterationToggle_CheckedChanged);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(488, 37);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(145, 19);
            this.metroLabel6.TabIndex = 12;
            this.metroLabel6.Text = "Condiciones de parada";
            this.metroLabel6.UseWaitCursor = true;
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(442, 83);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(59, 19);
            this.metroLabel7.TabIndex = 13;
            this.metroLabel7.Text = "Iteracion";
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(442, 117);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(117, 19);
            this.metroLabel8.TabIndex = 15;
            this.metroLabel8.Text = "Error Aproximado";
            // 
            // errorToggle
            // 
            this.errorToggle.AutoSize = true;
            this.errorToggle.Checked = true;
            this.errorToggle.CheckState = System.Windows.Forms.CheckState.Checked;
            this.errorToggle.Location = new System.Drawing.Point(565, 119);
            this.errorToggle.Name = "errorToggle";
            this.errorToggle.Size = new System.Drawing.Size(80, 17);
            this.errorToggle.TabIndex = 14;
            this.errorToggle.Text = "On";
            this.errorToggle.UseSelectable = true;
            this.errorToggle.CheckedChanged += new System.EventHandler(this.ErrorToggle_CheckedChanged);
            // 
            // methodSelector
            // 
            this.methodSelector.FormattingEnabled = true;
            this.methodSelector.ItemHeight = 23;
            this.methodSelector.Items.AddRange(new object[] {
            "Metodo de biseccion",
            "Metodo de falsa posicion"});
            this.methodSelector.Location = new System.Drawing.Point(442, 152);
            this.methodSelector.Name = "methodSelector";
            this.methodSelector.Size = new System.Drawing.Size(216, 29);
            this.methodSelector.TabIndex = 16;
            this.methodSelector.UseSelectable = true;
            // 
            // methodSelectorNR
            // 
            this.methodSelectorNR.Controls.Add(this.tabPage1);
            this.methodSelectorNR.Controls.Add(this.tabPage2);
            this.methodSelectorNR.Location = new System.Drawing.Point(14, 63);
            this.methodSelectorNR.Name = "methodSelectorNR";
            this.methodSelectorNR.SelectedIndex = 1;
            this.methodSelectorNR.Size = new System.Drawing.Size(680, 362);
            this.methodSelectorNR.TabIndex = 17;
            this.methodSelectorNR.UseSelectable = true;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.methodSelector);
            this.tabPage1.Controls.Add(this.metroLabel8);
            this.tabPage1.Controls.Add(this.iterationTxtBox);
            this.tabPage1.Controls.Add(this.errorToggle);
            this.tabPage1.Controls.Add(this.functionTxtBox);
            this.tabPage1.Controls.Add(this.metroLabel7);
            this.tabPage1.Controls.Add(this.metroLabel6);
            this.tabPage1.Controls.Add(this.xiTxtBox);
            this.tabPage1.Controls.Add(this.iterationToggle);
            this.tabPage1.Controls.Add(this.xuTxtBox);
            this.tabPage1.Controls.Add(this.errorTxtBox);
            this.tabPage1.Controls.Add(this.evaluateButton);
            this.tabPage1.Controls.Add(this.metroLabel1);
            this.tabPage1.Controls.Add(this.errorLabel);
            this.tabPage1.Controls.Add(this.metroLabel2);
            this.tabPage1.Controls.Add(this.iterationLabel);
            this.tabPage1.Controls.Add(this.metroLabel4);
            this.tabPage1.HorizontalScrollbarBarColor = true;
            this.tabPage1.HorizontalScrollbarHighlightOnWheel = false;
            this.tabPage1.HorizontalScrollbarSize = 10;
            this.tabPage1.Location = new System.Drawing.Point(4, 38);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(672, 320);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Metodos cerrados";
            this.tabPage1.VerticalScrollbarBarColor = true;
            this.tabPage1.VerticalScrollbarHighlightOnWheel = false;
            this.tabPage1.VerticalScrollbarSize = 10;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.evaluateButtonOpen);
            this.tabPage2.Controls.Add(this.iterationTxtBoxOpen);
            this.tabPage2.Controls.Add(this.functionTxtBoxOpen);
            this.tabPage2.Controls.Add(this.xiTxtBoxOpen);
            this.tabPage2.Controls.Add(this.errorTxtBoxOpen);
            this.tabPage2.Controls.Add(this.metroLabel10);
            this.tabPage2.Controls.Add(this.errorLabelOpen);
            this.tabPage2.Controls.Add(this.metroLabel12);
            this.tabPage2.Controls.Add(this.iterationLabelO);
            this.tabPage2.Controls.Add(this.methodSelectorOpen);
            this.tabPage2.Controls.Add(this.W);
            this.tabPage2.Controls.Add(this.errorToggleOpen);
            this.tabPage2.Controls.Add(this.iterationLabelOpen);
            this.tabPage2.Controls.Add(this.metroLabel9);
            this.tabPage2.Controls.Add(this.iterationToggleOpen);
            this.tabPage2.HorizontalScrollbarBarColor = true;
            this.tabPage2.HorizontalScrollbarHighlightOnWheel = false;
            this.tabPage2.HorizontalScrollbarSize = 10;
            this.tabPage2.Location = new System.Drawing.Point(4, 38);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(672, 320);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Metodos Abiertos";
            this.tabPage2.VerticalScrollbarBarColor = true;
            this.tabPage2.VerticalScrollbarHighlightOnWheel = false;
            this.tabPage2.VerticalScrollbarSize = 10;
            // 
            // evaluateButtonOpen
            // 
            this.evaluateButtonOpen.Location = new System.Drawing.Point(186, 279);
            this.evaluateButtonOpen.Name = "evaluateButtonOpen";
            this.evaluateButtonOpen.Size = new System.Drawing.Size(141, 23);
            this.evaluateButtonOpen.TabIndex = 33;
            this.evaluateButtonOpen.Text = "Evaluar";
            this.evaluateButtonOpen.UseSelectable = true;
            this.evaluateButtonOpen.Click += new System.EventHandler(this.EvaluateButtonOpen_Click);
            // 
            // iterationTxtBoxOpen
            // 
            // 
            // 
            // 
            this.iterationTxtBoxOpen.CustomButton.Image = null;
            this.iterationTxtBoxOpen.CustomButton.Location = new System.Drawing.Point(104, 1);
            this.iterationTxtBoxOpen.CustomButton.Name = "";
            this.iterationTxtBoxOpen.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.iterationTxtBoxOpen.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.iterationTxtBoxOpen.CustomButton.TabIndex = 1;
            this.iterationTxtBoxOpen.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.iterationTxtBoxOpen.CustomButton.UseSelectable = true;
            this.iterationTxtBoxOpen.CustomButton.Visible = false;
            this.iterationTxtBoxOpen.Lines = new string[] {
        "5"};
            this.iterationTxtBoxOpen.Location = new System.Drawing.Point(134, 106);
            this.iterationTxtBoxOpen.MaxLength = 32767;
            this.iterationTxtBoxOpen.Name = "iterationTxtBoxOpen";
            this.iterationTxtBoxOpen.PasswordChar = '\0';
            this.iterationTxtBoxOpen.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.iterationTxtBoxOpen.SelectedText = "";
            this.iterationTxtBoxOpen.SelectionLength = 0;
            this.iterationTxtBoxOpen.SelectionStart = 0;
            this.iterationTxtBoxOpen.ShortcutsEnabled = true;
            this.iterationTxtBoxOpen.Size = new System.Drawing.Size(126, 23);
            this.iterationTxtBoxOpen.TabIndex = 26;
            this.iterationTxtBoxOpen.Text = "5";
            this.iterationTxtBoxOpen.UseSelectable = true;
            this.iterationTxtBoxOpen.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.iterationTxtBoxOpen.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // functionTxtBoxOpen
            // 
            // 
            // 
            // 
            this.functionTxtBoxOpen.CustomButton.Image = null;
            this.functionTxtBoxOpen.CustomButton.Location = new System.Drawing.Point(104, 1);
            this.functionTxtBoxOpen.CustomButton.Name = "";
            this.functionTxtBoxOpen.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.functionTxtBoxOpen.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.functionTxtBoxOpen.CustomButton.TabIndex = 1;
            this.functionTxtBoxOpen.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.functionTxtBoxOpen.CustomButton.UseSelectable = true;
            this.functionTxtBoxOpen.CustomButton.Visible = false;
            this.functionTxtBoxOpen.Lines = new string[] {
        "-0.5x^2 + 2.5x + 4.5"};
            this.functionTxtBoxOpen.Location = new System.Drawing.Point(134, 26);
            this.functionTxtBoxOpen.MaxLength = 32767;
            this.functionTxtBoxOpen.Name = "functionTxtBoxOpen";
            this.functionTxtBoxOpen.PasswordChar = '\0';
            this.functionTxtBoxOpen.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.functionTxtBoxOpen.SelectedText = "";
            this.functionTxtBoxOpen.SelectionLength = 0;
            this.functionTxtBoxOpen.SelectionStart = 0;
            this.functionTxtBoxOpen.ShortcutsEnabled = true;
            this.functionTxtBoxOpen.Size = new System.Drawing.Size(126, 23);
            this.functionTxtBoxOpen.TabIndex = 23;
            this.functionTxtBoxOpen.Text = "-0.5x^2 + 2.5x + 4.5";
            this.functionTxtBoxOpen.UseSelectable = true;
            this.functionTxtBoxOpen.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.functionTxtBoxOpen.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // xiTxtBoxOpen
            // 
            // 
            // 
            // 
            this.xiTxtBoxOpen.CustomButton.Image = null;
            this.xiTxtBoxOpen.CustomButton.Location = new System.Drawing.Point(104, 1);
            this.xiTxtBoxOpen.CustomButton.Name = "";
            this.xiTxtBoxOpen.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.xiTxtBoxOpen.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.xiTxtBoxOpen.CustomButton.TabIndex = 1;
            this.xiTxtBoxOpen.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.xiTxtBoxOpen.CustomButton.UseSelectable = true;
            this.xiTxtBoxOpen.CustomButton.Visible = false;
            this.xiTxtBoxOpen.Lines = new string[] {
        "5"};
            this.xiTxtBoxOpen.Location = new System.Drawing.Point(134, 66);
            this.xiTxtBoxOpen.MaxLength = 32767;
            this.xiTxtBoxOpen.Name = "xiTxtBoxOpen";
            this.xiTxtBoxOpen.PasswordChar = '\0';
            this.xiTxtBoxOpen.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.xiTxtBoxOpen.SelectedText = "";
            this.xiTxtBoxOpen.SelectionLength = 0;
            this.xiTxtBoxOpen.SelectionStart = 0;
            this.xiTxtBoxOpen.ShortcutsEnabled = true;
            this.xiTxtBoxOpen.Size = new System.Drawing.Size(126, 23);
            this.xiTxtBoxOpen.TabIndex = 24;
            this.xiTxtBoxOpen.Text = "5";
            this.xiTxtBoxOpen.UseSelectable = true;
            this.xiTxtBoxOpen.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.xiTxtBoxOpen.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // errorTxtBoxOpen
            // 
            // 
            // 
            // 
            this.errorTxtBoxOpen.CustomButton.Image = null;
            this.errorTxtBoxOpen.CustomButton.Location = new System.Drawing.Point(104, 1);
            this.errorTxtBoxOpen.CustomButton.Name = "";
            this.errorTxtBoxOpen.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.errorTxtBoxOpen.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.errorTxtBoxOpen.CustomButton.TabIndex = 1;
            this.errorTxtBoxOpen.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.errorTxtBoxOpen.CustomButton.UseSelectable = true;
            this.errorTxtBoxOpen.CustomButton.Visible = false;
            this.errorTxtBoxOpen.Lines = new string[] {
        "3"};
            this.errorTxtBoxOpen.Location = new System.Drawing.Point(134, 147);
            this.errorTxtBoxOpen.MaxLength = 32767;
            this.errorTxtBoxOpen.Name = "errorTxtBoxOpen";
            this.errorTxtBoxOpen.PasswordChar = '\0';
            this.errorTxtBoxOpen.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.errorTxtBoxOpen.SelectedText = "";
            this.errorTxtBoxOpen.SelectionLength = 0;
            this.errorTxtBoxOpen.SelectionStart = 0;
            this.errorTxtBoxOpen.ShortcutsEnabled = true;
            this.errorTxtBoxOpen.Size = new System.Drawing.Size(126, 23);
            this.errorTxtBoxOpen.TabIndex = 27;
            this.errorTxtBoxOpen.Text = "3";
            this.errorTxtBoxOpen.UseSelectable = true;
            this.errorTxtBoxOpen.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.errorTxtBoxOpen.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(13, 26);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(54, 19);
            this.metroLabel10.TabIndex = 28;
            this.metroLabel10.Text = "Funcion";
            // 
            // errorLabelOpen
            // 
            this.errorLabelOpen.AutoSize = true;
            this.errorLabelOpen.Location = new System.Drawing.Point(11, 147);
            this.errorLabelOpen.Name = "errorLabelOpen";
            this.errorLabelOpen.Size = new System.Drawing.Size(117, 19);
            this.errorLabelOpen.TabIndex = 32;
            this.errorLabelOpen.Text = "Error Aproximado";
            // 
            // metroLabel12
            // 
            this.metroLabel12.AutoSize = true;
            this.metroLabel12.Location = new System.Drawing.Point(11, 66);
            this.metroLabel12.Name = "metroLabel12";
            this.metroLabel12.Size = new System.Drawing.Size(20, 19);
            this.metroLabel12.TabIndex = 29;
            this.metroLabel12.Text = "Xi";
            // 
            // iterationLabelO
            // 
            this.iterationLabelO.AutoSize = true;
            this.iterationLabelO.Location = new System.Drawing.Point(11, 106);
            this.iterationLabelO.Name = "iterationLabelO";
            this.iterationLabelO.Size = new System.Drawing.Size(71, 19);
            this.iterationLabelO.TabIndex = 31;
            this.iterationLabelO.Text = "Iteraciones";
            // 
            // methodSelectorOpen
            // 
            this.methodSelectorOpen.AutoCompleteCustomSource.AddRange(new string[] {
            "Newton Raphson"});
            this.methodSelectorOpen.FormattingEnabled = true;
            this.methodSelectorOpen.ItemHeight = 23;
            this.methodSelectorOpen.Items.AddRange(new object[] {
            "Newton Raphson"});
            this.methodSelectorOpen.Location = new System.Drawing.Point(444, 145);
            this.methodSelectorOpen.Name = "methodSelectorOpen";
            this.methodSelectorOpen.Size = new System.Drawing.Size(216, 29);
            this.methodSelectorOpen.TabIndex = 22;
            this.methodSelectorOpen.UseSelectable = true;
            // 
            // W
            // 
            this.W.AutoSize = true;
            this.W.Location = new System.Drawing.Point(444, 110);
            this.W.Name = "W";
            this.W.Size = new System.Drawing.Size(117, 19);
            this.W.TabIndex = 21;
            this.W.Text = "Error Aproximado";
            // 
            // errorToggleOpen
            // 
            this.errorToggleOpen.AutoSize = true;
            this.errorToggleOpen.Checked = true;
            this.errorToggleOpen.CheckState = System.Windows.Forms.CheckState.Checked;
            this.errorToggleOpen.Location = new System.Drawing.Point(567, 112);
            this.errorToggleOpen.Name = "errorToggleOpen";
            this.errorToggleOpen.Size = new System.Drawing.Size(80, 17);
            this.errorToggleOpen.TabIndex = 20;
            this.errorToggleOpen.Text = "On";
            this.errorToggleOpen.UseSelectable = true;
            this.errorToggleOpen.CheckedChanged += new System.EventHandler(this.ErrorToggleOpen_CheckedChanged);
            // 
            // iterationLabelOpen
            // 
            this.iterationLabelOpen.AutoSize = true;
            this.iterationLabelOpen.Location = new System.Drawing.Point(444, 76);
            this.iterationLabelOpen.Name = "iterationLabelOpen";
            this.iterationLabelOpen.Size = new System.Drawing.Size(59, 19);
            this.iterationLabelOpen.TabIndex = 19;
            this.iterationLabelOpen.Text = "Iteracion";
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(490, 30);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(145, 19);
            this.metroLabel9.TabIndex = 18;
            this.metroLabel9.Text = "Condiciones de parada";
            this.metroLabel9.UseWaitCursor = true;
            // 
            // iterationToggleOpen
            // 
            this.iterationToggleOpen.AutoSize = true;
            this.iterationToggleOpen.Checked = true;
            this.iterationToggleOpen.CheckState = System.Windows.Forms.CheckState.Checked;
            this.iterationToggleOpen.Location = new System.Drawing.Point(567, 72);
            this.iterationToggleOpen.Name = "iterationToggleOpen";
            this.iterationToggleOpen.Size = new System.Drawing.Size(80, 17);
            this.iterationToggleOpen.TabIndex = 17;
            this.iterationToggleOpen.Text = "On";
            this.iterationToggleOpen.UseSelectable = true;
            this.iterationToggleOpen.CheckedChanged += new System.EventHandler(this.IterationToggleOpen_CheckedChanged);
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(717, 448);
            this.Controls.Add(this.methodSelectorNR);
            this.Name = "MainView";
            this.Text = "Biseccion";
            this.Load += new System.EventHandler(this.MainView_Load);
            this.methodSelectorNR.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox functionTxtBox;
        private MetroFramework.Controls.MetroTextBox xiTxtBox;
        private MetroFramework.Controls.MetroTextBox iterationTxtBox;
        private MetroFramework.Controls.MetroTextBox xuTxtBox;
        private MetroFramework.Controls.MetroTextBox errorTxtBox;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel iterationLabel;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel errorLabel;
        private MetroFramework.Controls.MetroButton evaluateButton;
        private MetroFramework.Controls.MetroToggle iterationToggle;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroToggle errorToggle;
        private MetroFramework.Controls.MetroComboBox methodSelector;
        private MetroFramework.Controls.MetroTabControl methodSelectorNR;
        private MetroFramework.Controls.MetroTabPage tabPage1;
        private MetroFramework.Controls.MetroTabPage tabPage2;
        private MetroFramework.Controls.MetroComboBox methodSelectorOpen;
        private MetroFramework.Controls.MetroLabel W;
        private MetroFramework.Controls.MetroToggle errorToggleOpen;
        private MetroFramework.Controls.MetroLabel iterationLabelOpen;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroToggle iterationToggleOpen;
        private MetroFramework.Controls.MetroButton evaluateButtonOpen;
        private MetroFramework.Controls.MetroTextBox iterationTxtBoxOpen;
        private MetroFramework.Controls.MetroTextBox functionTxtBoxOpen;
        private MetroFramework.Controls.MetroTextBox xiTxtBoxOpen;
        private MetroFramework.Controls.MetroTextBox errorTxtBoxOpen;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroLabel errorLabelOpen;
        private MetroFramework.Controls.MetroLabel metroLabel12;
        private MetroFramework.Controls.MetroLabel iterationLabelO;
    }
}

