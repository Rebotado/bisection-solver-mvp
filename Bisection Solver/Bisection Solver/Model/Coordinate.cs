﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bisection_Solver.Model
{
    struct Coordinate
    {
        private readonly double x;
        private readonly double y;

        public double X { get { return x; } }
        public double Y { get { return y; } }

        public Coordinate(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public override string ToString()
        {
            return string.Format("{0} , {1}", X, Y);
        }

    }
}
