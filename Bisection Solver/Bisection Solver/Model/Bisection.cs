﻿using org.mariuszgromada.math.mxparser;
using System;
using System.Collections.Generic;

namespace Bisection_Solver.Model
{
    public class Bisection : CloseMethodSolver
    {
        public Bisection() { }
        public Bisection(double xi, double xu, double approximateError, string functionString, int iteration = 0)
        {
            Xi = xi;
            Xu = xu;
            LastXi = xi;
            LastXu = xu;
            Iteration = iteration;
            ApproximateError = approximateError;
            DefinedFunction = new Function(functionString);
        }


        public override List<string> GetResultList()
        {
            double xrResult;
            double xiResult;
            double error;
            List<string> results = new List<string>();


            for (int i = 1; i <= Iteration; i++)
            {
                double newXr = (LastXi + LastXu) / 2;
                error = Math.Round(Math.Abs(((newXr - Xr) / newXr) * 100), 5);
                Xr = newXr;
                xrResult = EvaluateFunction(Xr);
                xiResult = EvaluateFunction(LastXi);
                Sign = xrResult * xiResult > 0 ? '+' : '-';
                results.Add(GetResultString(i, xiResult, default , xrResult, error >= 100 ? double.NaN : error));
                if (Sign == '-')
                    LastXu = Xr;
                else
                    LastXi = Xr;

                if (i >= Iteration || error <= ApproximateError)
                {
                    ResultSentence = GetResultSentence(i, error);
                    break;
                }
            }
            return results;
        }
        public override string GetResultString(int counter, double xiResult, double xuResult, double xrResult, double currentError)
        {
            return $"|{counter}|{LastXi}|{LastXu}|{xiResult}|{Xr}|{xrResult}|{Sign}|{currentError}%|";
        }
    }
}
