﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using org.mariuszgromada.math.mxparser;
using System.Diagnostics;


namespace Bisection_Solver.Model
{
    public class NewtonRaphson
    {
        private Argument x;
        private double error;
        public Function Function { get; set; }
        public Expression DerivedFunction { get; set; }

        public double Xi { get; set; }
        public double PreviousXi { get; set; }
        public double LastXi { get; set; }
        public int Iteration { get; set; }
        public double LimitError { get; set; }

        public double Error {
            get { return error; }
            private set
            {
                error = value < 0 ? Double.NaN : value;
            }
        }

        public List<string> Results { get; private set; }
        public string ResultString { get; private set; }



        public NewtonRaphson() { }
        public NewtonRaphson(string functionString, int iteration, double limitError, double xi)
        {
            Function = new Function(functionString);
            Iteration = iteration;
            Results = new List<string>();
            x = new Argument("x");
            DerivedFunction = new Expression($"der({Function.getFunctionExpressionString()},x)", x);
            LimitError = limitError;
            Xi = xi;
            LastXi = xi;
        }

        public void Evaluate()
        {
            for(int i = 0; i < Iteration; i++)
            {
                double fx = Function.calculate(LastXi);
                x.setArgumentValue(LastXi);
                double derfx = DerivedFunction.calculate();
                PreviousXi = LastXi;
                LastXi = LastXi - (fx / derfx);
                Error = GetError();
                Results.Add(GetResultString(fx, derfx));
                ResultString = GetResultSentence(i+1);
                if (Error <= LimitError)
                    break;
            }
        }

        private double GetError()
        {
            return ((PreviousXi - LastXi) / PreviousXi) * 100;
        }

        private string GetResultString(double fx, double derfx)
        {
            return $"{PreviousXi}|{fx}|{derfx}|{LastXi}|{Error}";
        }

        private string GetResultSentence(int lastIteration)
        {
            return $"La tolerancia del error fue staisfecho en la iteracion {lastIteration} con un valor de X: {LastXi} y Error Aproximado del: {Error}%";

        }




    }
}
