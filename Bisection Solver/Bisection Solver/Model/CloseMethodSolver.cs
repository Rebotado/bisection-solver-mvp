﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using org.mariuszgromada.math.mxparser;


namespace Bisection_Solver.Model
{
    public abstract class CloseMethodSolver
    {
        private double approximateError;
        private int iteration;

        public double Xi { get; set; }
        public double Xu { get; set; }
        public double LastXi { get; set; }
        public double LastXu { get; set; }
        public double Xr { get; set; }
        public double ApproximateError
        {
            get { return approximateError; }
            set { approximateError = value > 100 ? 100 : value < 0 ? 0 : value; }
        }
        public int Iteration
        {
            get { return iteration; }
            set
            {
                iteration = value == 0 || value > 1000 ? 1000 : value < 0 ? 0 : value;
            }
        }
        public char Sign { get; set; }
        public Function DefinedFunction { get; set; }
        public string ResultSentence { get; set; }


        public  abstract List<string> GetResultList();
        public double EvaluateFunction(double variableValue)
        {
            return DefinedFunction.calculate(variableValue);
        }
        public abstract string GetResultString(int counter, double xiResult, double xuResult, double xrResult, double currentError);
        public string GetResultSentence(int lastIteration, double currentError)
        {
            return $"La tolerancia del error fue staisfecho en la iteracion {lastIteration} con un valor de Xr: {Xr} y Error Aproximado del: {currentError}%";

        }

    }
}
