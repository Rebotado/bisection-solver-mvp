﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using org.mariuszgromada.math.mxparser;


namespace Bisection_Solver.Model
{
    class FalsePosition : CloseMethodSolver
    {
        public FalsePosition() { }
        public FalsePosition(double xi, double xu, double approximateError, string functionString, int iteration = 0)
        {
            Xi = xi;
            Xu = xu;
            LastXi = xi;
            LastXu = xu;
            Iteration = iteration;
            ApproximateError = approximateError;
            DefinedFunction = new Function(functionString);
        }

        public override List<string> GetResultList()
        {
            double xrResult;
            double xiResult;
            double xuResult;
            double error;
            List<string> results = new List<string>();

            for(int i = 1; i <= Iteration; i++)
            {
                xiResult = EvaluateFunction(LastXi);
                xuResult = EvaluateFunction(LastXu);
                double newXr = LastXu - ((xuResult * (LastXi - LastXu)) / (xiResult - xuResult));
                error = Math.Round(Math.Abs(((newXr - Xr) / newXr) * 100), 5);
                Xr = newXr;
                xrResult = EvaluateFunction(Xr);
                Sign = xrResult * xiResult > 0 ? '+' : '-';
                results.Add(GetResultString(i, xiResult, xuResult, xrResult, error >= 100 ? double.NaN : error));
                if (Sign == '-')
                    LastXu = Xr;
                else
                    LastXi = Xr;
                if (i >= Iteration || error <= ApproximateError)
                {
                    ResultSentence = GetResultSentence(i, error);
                    break;
                }
            }
                return results;

        }

        public override string GetResultString(int counter, double xiResult, double xuResult, double xrResult, double currentError)
        {
            return $"|{counter}|{LastXi}|{xiResult}|{LastXu}|{xuResult}|{Xr}|{xrResult}|{Sign}|{currentError}%|";
        }
    }
}
