﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bisection_Solver.View;
using Bisection_Solver.Model;
using Bisection_Solver.Presenter;

namespace Bisection_Solver
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var view = new MainView();
            var model = new Bisection();
            var model2 = new NewtonRaphson();
            var presenter = new BisectionPresenter(model2, model, view);
            Application.Run(view);
        }
    }
}
